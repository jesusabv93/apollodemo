package com.jesusabv93.apollodemo.data

import com.jesusabv93.apollodemo.ui.adapter.Adapter
import com.jesusabv93.apollodemo.ui.utils.interfaces.ViewType

data class FilmsPage(
        var totalCount: Long = 0,
        var pageInfo: PageInfo = PageInfo(),
        var films: List<Film> = listOf()
)

data class PageInfo(
        var startCursor: String = "",
        var endCursor: String = "",
        var hasNextPage: Boolean = false,
        var hasPreviousPage: Boolean = false
)

data class Film(
        var id: String = "",
        var title: String = "",
        var episodeID: Long = 0,
        var openingCrawl: String = "",
        var director: String = "",
        var producers: List<String> = listOf(),
        var releaseDate: String = "",
        var characters: List<Character> = listOf(),
        var starships: List<Starship> = listOf(),
        var created: String = "",
        var edited: String = ""
) : ViewType {
    override fun getViewType() = Adapter.FILM
}

data class Character(
        var id: String = "",
        var name: String = "",
        var birthYear: String = "",
        var eyeColor: String = "",
        var gender: String = "",
        var hairColor: String = "",
        var height: Long = 0,
        var mass: Double = 0.toDouble(),
        var skinColor: String = "",
        var vehicles: List<Vehicle> = listOf()
)

data class Vehicle(
        var name: String = "",
        var model: String = "",
        var vehicleClass: String = "",
        var cargoCapacity: Double = 0.toDouble()
)

data class Starship(
        var id: String = "",
        var name: String = "",
        var model: String = ""
)

data class PeoplePage(
        var totalCount: Long = 0,
        var pageInfo: PageInfo = PageInfo(),
        var people: List<Person> = listOf()
)

data class Person(
        var id: String = "",
        var name: String = "",
        var birthYear: String = "",
        var eyeColor: String = "",
        var gender: String = "",
        var hairColor: String = "",
        var height: Long = 0,
        var mass: Double = 0.toDouble(),
        var skinColor: String = "",
        var vehicles: List<Vehicle> = listOf()
) : ViewType {
    override fun getViewType() = Adapter.PERSON
}