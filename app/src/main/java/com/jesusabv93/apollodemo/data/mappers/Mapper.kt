package com.jesusabv93.apollodemo.data.mappers

import com.jesusabv93.apollodemo.ui.utils.interfaces.Logger

abstract class Mapper<T1, T2> : Logger {

    abstract fun map(value: T1): T2

    fun map(values: Collection<T1>): List<T2> = values.map { map(it) }

}