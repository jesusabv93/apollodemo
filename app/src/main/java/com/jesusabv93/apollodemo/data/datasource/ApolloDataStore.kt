package com.jesusabv93.apollodemo.data.datasource

import com.jesusabv93.apollodemo.AllFilmsQuery
import com.jesusabv93.apollodemo.AllPeopleQuery
import com.jesusabv93.apollodemo.data.mappers.FilmsPageMapper
import com.jesusabv93.apollodemo.data.mappers.PeoplePageMapper
import com.jesusabv93.apollodemo.data.FilmsPage
import com.jesusabv93.apollodemo.data.PeoplePage

class ApolloDataStore : DataStore {

    lateinit var filmsPageMapper: FilmsPageMapper
    lateinit var peoplePageMapper: PeoplePageMapper

    override suspend fun getFilms(first: Long?, after: String?, last: Long?, before: String?): FilmsPage {
        try {
            val response = ApolloClient.getApolloClient()
                    .query(AllFilmsQuery.builder()
                            .first(first)
                            .after(after)
                            .last(last)
                            .before(before)
                            .build()
                    ).execute()
            val data = response.data()?.allFilms()
            if (data != null) {
                return filmsPageMapper.map(data)
            } else {
                throw Exception("Ocurrió un error obteniendo las películas")
            }
        } catch (ex: Exception) {
            throw ex
        }
    }

    override suspend fun getPeople(first: Long?, after: String?, last: Long?, before: String?): PeoplePage {
        try {
            val response = ApolloClient.getApolloClient()
                    .query(AllPeopleQuery.builder()
                            .first(first)
                            .after(after)
                            .last(last)
                            .before(before)
                            .build()
                    ).execute()
            val data = response.data()?.allPeople()
            if (data != null) {
                return peoplePageMapper.map(data)
            } else {
                throw Exception("Ocurrió un error obteniendo los personajes")
            }
        } catch (ex: Exception) {
            throw ex
        }
    }

}