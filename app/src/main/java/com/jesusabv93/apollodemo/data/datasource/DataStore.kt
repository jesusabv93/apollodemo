package com.jesusabv93.apollodemo.data.datasource

import com.jesusabv93.apollodemo.data.FilmsPage
import com.jesusabv93.apollodemo.data.PeoplePage

interface DataStore {

    suspend fun getFilms(first: Long? = null, after: String? = null, last: Long? = null, before: String? = null): FilmsPage

    suspend fun getPeople(first: Long? = null, after: String? = null, last: Long? = null, before: String? = null): PeoplePage

}