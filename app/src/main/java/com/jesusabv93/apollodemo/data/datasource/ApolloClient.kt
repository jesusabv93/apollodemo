package com.jesusabv93.apollodemo.data.datasource

import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.jesusabv93.apollodemo.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit
import kotlin.coroutines.experimental.suspendCoroutine


class ApolloClient {

    companion object {
        fun getApolloClient() = ApolloClient.builder()
                .serverUrl(BuildConfig.DomainApi)
                .okHttpClient(OkHttpClient.Builder()
                        .addInterceptor(HttpLoggingInterceptor())
                        .readTimeout(30, TimeUnit.SECONDS)
                        .connectTimeout(30, TimeUnit.SECONDS)
                        .writeTimeout(30, TimeUnit.SECONDS)
                        .build())
                .build()

    }
}

suspend fun <T> ApolloCall<T>.execute() = suspendCoroutine<Response<T>> { cont ->
    enqueue(object : ApolloCall.Callback<T>() {
        override fun onResponse(response: Response<T>) {
            cont.resume(response)
        }
        override fun onFailure(e: ApolloException) {
            e.printStackTrace()
            cont.resumeWithException(e)
        }
    })
}