package com.jesusabv93.apollodemo.data.mappers

import com.jesusabv93.apollodemo.data.PageInfo
import com.jesusabv93.apollodemo.fragment.PageInfoFragment

class PageInfoMapper: Mapper<PageInfoFragment, PageInfo> (){

    override fun map(value: PageInfoFragment)= PageInfo(
            value.startCursor() ?: "", value.endCursor()
            ?: "", value.hasNextPage(), value.hasPreviousPage()
    )
}