package com.jesusabv93.apollodemo.data.mappers

import com.jesusabv93.apollodemo.AllFilmsQuery
import com.jesusabv93.apollodemo.AllPeopleQuery
import com.jesusabv93.apollodemo.data.*
import com.jesusabv93.apollodemo.fragment.PersonFragment
import com.jesusabv93.apollodemo.fragment.StarshipFragment

class FilmsPageMapper(private val pageInfoMapper: PageInfoMapper, private val filmMapper: FilmMapper) : Mapper<AllFilmsQuery.AllFilms, FilmsPage>() {

    override fun map(value: AllFilmsQuery.AllFilms) = FilmsPage(value.totalCount()
            ?: 0,
            pageInfoMapper.map(value.pageInfo().fragments().pageInfoFragment()),
            filmMapper.map(value.edges()?.mapNotNull { it.node() } ?: listOf())
    )
}

class FilmMapper(private val characterMapper: CharacterMapper, private val starshipMapper: StarshipMapper) : Mapper<AllFilmsQuery.Node, Film>() {

    override fun map(value: AllFilmsQuery.Node) = Film(
            value.id(), value.title() ?: "", value.episodeID() ?: 0, value.openingCrawl()
            ?: "", value.director() ?: "", value.producers() ?: listOf(), value.releaseDate() ?: "",
            characterMapper.map(value.characterConnection()?.edges()?.mapNotNull { it.node()?.fragments()?.personFragment() }
                    ?: listOf()),
            starshipMapper.map(value.starshipConnection()?.edges()?.mapNotNull { it.node()?.fragments()?.starshipFragment() }
                    ?: listOf()),
            value.created() ?: "", value.edited() ?: ""
    )
}

class CharacterMapper(private val vehicleMapper: VehicleMapper) : Mapper<PersonFragment, Character>() {
    override fun map(value: PersonFragment) = Character(
            value.id(), value.name() ?: "", value.birthYear() ?: "", value.eyeColor()
            ?: "", value.gender() ?: "",
            value.hairColor() ?: "", value.height() ?: 0,
            value.mass() ?: 0.toDouble(), value.skinColor() ?: "",
            vehicleMapper.map(value.vehicleConnection()?.vehicles()?.mapNotNull { it } ?: listOf())
    )
}

class VehicleMapper : Mapper<PersonFragment.Vehicle, Vehicle>() {
    override fun map(value: PersonFragment.Vehicle) = Vehicle(value.name()
            ?: "",
            value.model() ?: "", value.vehicleClass() ?: "",
            value.cargoCapacity() ?: 0.toDouble()
    )
}

class StarshipMapper : Mapper<StarshipFragment, Starship>() {
    override fun map(value: StarshipFragment) = Starship(
            value.id(), value.name() ?: "", value.model() ?: ""
    )
}

class PeoplePageMapper(private val pageInfoMapper: PageInfoMapper, private val personMapper: PersonMapper) : Mapper<AllPeopleQuery.AllPeople, PeoplePage>() {
    override fun map(value: AllPeopleQuery.AllPeople) = PeoplePage(value.totalCount()
            ?: 0,
            pageInfoMapper.map(value.pageInfo().fragments().pageInfoFragment()),
            personMapper.map(value.edges()?.mapNotNull { it.node()?.fragments()?.personFragment() }
                    ?: listOf())
    )
}

class PersonMapper(private val vehicleMapper: VehicleMapper) : Mapper<PersonFragment, Person>() {
    override fun map(value: PersonFragment) = Person(value.id(), value.name()
            ?: "",
            value.birthYear() ?: "", value.eyeColor() ?: "",
            value.gender() ?: "", value.hairColor() ?: "",
            value.height() ?: 0, value.mass() ?: 0.toDouble(),
            value.skinColor() ?: "",
            vehicleMapper.map(value.vehicleConnection()?.vehicles()?.mapNotNull { it } ?: listOf())
    )
}