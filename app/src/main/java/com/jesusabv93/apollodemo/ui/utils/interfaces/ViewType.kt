package com.jesusabv93.apollodemo.ui.utils.interfaces

interface ViewType{
    fun getViewType(): Int
}