package com.jesusabv93.apollodemo.ui.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.PagerAdapter

class ViewPagerAdapter(manager: FragmentManager) : FragmentStatePagerAdapter(manager) {

    private var baseId: Long = 0
    private var mFragmentList: MutableList<Fragment> = ArrayList()
    private var mFragmentTitleList: MutableList<String> = ArrayList()

    override fun getItem(position: Int): Fragment = mFragmentList[position]

    override fun getCount(): Int = mFragmentList.size

    fun addFragment(fragment: Fragment, title: String) {
        this.mFragmentList.add(fragment)
        this.mFragmentTitleList.add(title)
        notifyDataSetChanged()
    }

    override fun getPageTitle(position: Int): String = mFragmentTitleList[position]

    fun clear() {
        this.mFragmentList = java.util.ArrayList<Fragment>()
        this.mFragmentTitleList = java.util.ArrayList<String>()
        notifyDataSetChanged()
    }

    override fun getItemPosition(item: Any) = PagerAdapter.POSITION_NONE

    fun notifyChangeInPosition(n: Int) {
        baseId += (count + n).toLong()
    }

}