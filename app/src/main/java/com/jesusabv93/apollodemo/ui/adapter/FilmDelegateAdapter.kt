package com.jesusabv93.apollodemo.ui.adapter

import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.ViewGroup
import com.jesusabv93.apollodemo.R
import com.jesusabv93.apollodemo.data.Film
import com.jesusabv93.apollodemo.ui.utils.interfaces.ViewType
import com.jesusabv93.apollodemo.ui.utils.interfaces.ViewTypeDelegateAdapter
import com.jesusabv93.apollodemo.ui.utils.inflate
import kotlinx.android.synthetic.main.row_film.view.*

class FilmDelegateAdapter : ViewTypeDelegateAdapter {
    override fun onCreateViewHolder(parent: ViewGroup): ViewHolder {
        return FilmViewHolder(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, item: ViewType) {
        (holder as FilmViewHolder).bind(item as Film)
    }

    inner class FilmViewHolder(parent: ViewGroup) : ViewHolder(parent.inflate(R.layout.row_film)) {

        private var tvTitle = itemView.tvTitle
        private var tvEpisodeId = itemView.tvEpisodeId
        private var tvOpeningCrawl = itemView.tvOpeningCrawl
        private var tvDirector = itemView.tvDirector

        fun bind(item: Film) {
            with(item) {
                tvTitle.text = title
                tvEpisodeId.text = "Episode $episodeID"
                tvOpeningCrawl.text = openingCrawl
                tvDirector.text = director
            }
        }
    }

}