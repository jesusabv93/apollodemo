package com.jesusabv93.apollodemo.ui.adapter

import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.ViewGroup
import com.jesusabv93.apollodemo.R
import com.jesusabv93.apollodemo.data.Person
import com.jesusabv93.apollodemo.ui.utils.interfaces.ViewType
import com.jesusabv93.apollodemo.ui.utils.interfaces.ViewTypeDelegateAdapter
import com.jesusabv93.apollodemo.ui.utils.inflate
import kotlinx.android.synthetic.main.row_person.view.*

class PersonDelegateAdapter : ViewTypeDelegateAdapter {
    override fun onCreateViewHolder(parent: ViewGroup): ViewHolder {
        return FilmViewHolder(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, item: ViewType) {
        (holder as FilmViewHolder).bind(item as Person)
    }

    inner class FilmViewHolder(parent: ViewGroup) : ViewHolder(parent.inflate(R.layout.row_person)) {

        private var tvName = itemView.tvName
        private var tvGender = itemView.tvGender

        fun bind(item: Person) {
            tvName.text = item.name
            tvGender.text = item.gender
        }

    }
}