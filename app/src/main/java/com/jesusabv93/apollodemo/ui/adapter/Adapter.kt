package com.jesusabv93.apollodemo.ui.adapter

import android.support.v4.util.SparseArrayCompat
import android.support.v7.widget.RecyclerView.Adapter
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.ViewGroup
import com.jesusabv93.apollodemo.ui.utils.interfaces.ViewType
import com.jesusabv93.apollodemo.ui.utils.interfaces.ViewTypeDelegateAdapter

class Adapter(val items: List<ViewType>): Adapter<ViewHolder>() {

    init {

    }

    companion object {
        val FILM = 1000
        val PERSON = 2000
    }

    private var delegateAdapters = SparseArrayCompat<ViewTypeDelegateAdapter>().apply {
        put(FILM, FilmDelegateAdapter())
        put(PERSON, PersonDelegateAdapter())
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            = delegateAdapters.get(viewType).onCreateViewHolder(parent)

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        delegateAdapters.get(getItemViewType(position)).onBindViewHolder(holder, items[position])
    }

    override fun getItemViewType(position: Int) = items[position].getViewType()

}