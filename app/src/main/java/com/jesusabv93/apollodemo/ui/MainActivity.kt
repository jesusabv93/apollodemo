package com.jesusabv93.apollodemo.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.jesusabv93.apollodemo.R
import com.jesusabv93.apollodemo.ui.adapter.ViewPagerAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewPager.adapter = ViewPagerAdapter(supportFragmentManager).apply {
            addFragment(FragmentList.newInstance(Bundle().apply {
                putSerializable(FragmentList.TYPE, Type.FILMS)
            }), getString(R.string.tab_title_films))
            addFragment(FragmentList.newInstance(Bundle().apply {
                putSerializable(FragmentList.TYPE, Type.PEOPLE)
            }), getString(R.string.tab_title_people))
        }

        tabLayout.setupWithViewPager(viewPager)


    }





}
