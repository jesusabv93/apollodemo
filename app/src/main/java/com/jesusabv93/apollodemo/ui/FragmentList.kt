package com.jesusabv93.apollodemo.ui

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jesusabv93.apollodemo.R
import com.jesusabv93.apollodemo.data.Film
import com.jesusabv93.apollodemo.data.datasource.ApolloDataStore
import com.jesusabv93.apollodemo.data.mappers.*
import com.jesusabv93.apollodemo.ui.adapter.Adapter
import com.jesusabv93.apollodemo.ui.utils.inflate
import kotlinx.android.synthetic.main.fragment_list.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import com.jesusabv93.apollodemo.ui.Type.FILMS
import com.jesusabv93.apollodemo.ui.Type.PEOPLE
import com.jesusabv93.apollodemo.ui.utils.toast

class FragmentList : Fragment() {

    companion object {
        fun newInstance(bundle: Bundle? = Bundle()) = FragmentList().apply {
            arguments = bundle
        }

        val TYPE = "type"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_list)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupRecyclerview()
        loadData(arguments?.getSerializable(TYPE) as Type)
    }


    private fun setupRecyclerview() {
        rvList.run {
            layoutManager = LinearLayoutManager(activity)
            adapter = Adapter(listOf())
        }
    }

    private fun loadData(type: Type) {
        async(UI) {
            try {
                rvList.adapter = Adapter(when (type) {
                    FILMS -> ApolloDataStore().apply {
                        filmsPageMapper = FilmsPageMapper(PageInfoMapper(), FilmMapper(CharacterMapper(VehicleMapper()), StarshipMapper()))
                    }.getFilms().films.sortedWith(compareBy { it.episodeID })
                    PEOPLE -> ApolloDataStore().apply {
                        peoplePageMapper = PeoplePageMapper(PageInfoMapper(), PersonMapper(VehicleMapper()))
                    }.getPeople().people
                })
                progressBar.visibility = View.GONE
            } catch (ex: Exception) {
                progressBar.visibility = View.GONE
                (this@FragmentList.activity as Context).toast(ex.message ?: "")
            }
        }
    }

}