package com.jesusabv93.apollodemo.ui

enum class Type {
    FILMS, PEOPLE
}