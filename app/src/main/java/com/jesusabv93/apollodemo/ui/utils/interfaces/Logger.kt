package com.jesusabv93.apollodemo.ui.utils.interfaces

import android.util.Log

interface Logger {

    val TAG: String
        get() = javaClass.simpleName

    fun d(message: String) = Log.e(TAG, message)

}