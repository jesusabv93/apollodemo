package com.jesusabv93.apollodemo.ui.utils.interfaces

import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.ViewGroup

interface ViewTypeDelegateAdapter: Logger {
    fun onCreateViewHolder(parent: ViewGroup): ViewHolder
    fun onBindViewHolder(holder: ViewHolder, item: ViewType)
}